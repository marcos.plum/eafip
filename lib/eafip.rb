require "eafip/version"
require 'eafip/constants'
require 'savon'
require 'eafip/core_ext/hash'
require 'eafip/core_ext/string'
require 'eafip/exceptions'
require 'eafip/eiva'
module Eafip

  extend self
  attr_accessor :openssl_bin

  # Exception Class for missing or invalid attributes
  #
  class NullOrInvalidAttribute < StandardError; end

  # Exception Class for missing or invalid certifficate
  #
  class MissingCertificate < StandardError; end

   # This class handles the logging options
  #
  class Logger < Struct.new(:log, :pretty_xml, :level)
    # @param opts [Hash] receives a hash with keys `log`, `pretty_xml` (both
    # boolean) or the desired log level as `level`

    def initialize(opts = {})
      self.log = opts[:log] || false
      self.pretty_xml = opts[:pretty_xml] || log
      self.level = opts[:level] || :debug
    end

    # @return [Hash] returns a hash with the proper logging optios for Savon.
    def logger_options
      { log: log, pretty_print_xml: pretty_xml, log_level: level, ssl_verify_mode: :none }
    end
  end

  # Your code goes here...
  autoload :Wsaa,         'eafip/wsaa'
  autoload :Company,      'eafip/company'
  autoload :Constants,    'eafip/constants'
  autoload :AuthData,     'eafip/auth_data'
  autoload :Bill,         'eafip/bill'
  autoload :Wsaa,         'eafip/wsaa'
  autoload :Reference,    'eafip/reference'
  autoload :Eiva,         'eafip/eiva'

  class << self
    # Receiver of the logging configuration options.
    # @param opts [Hash] pass a hash with `log`, `pretty_xml` and `level` keys to set
    # them.
    def logger=(opts)
      @logger ||= Logger.new(opts)
    end

    # Sets the logger options to the default values or returns the previously set
    # logger options
    # @return [Logger]
    def logger
      @logger ||= Logger.new
    end

    # Returs the formatted logger options to be used by Savon.
    def logger_options
      logger.logger_options
    end
  end
end
