module Eafip
  # Class in charge of issuing read requests on the api
  #
  class Reference
    # Fetches the number for the next bill to be issued
    # @return [Integer] the number for the next bill
    #
    attr_accessor :company

    def initialize(company)
      @company = company
    end

    def next_bill_number(cbte_type)
      set_client
      resp = @client.call(:fe_comp_ultimo_autorizado) do |soap|
        # soap.namespaces['xmlns'] = 'http://ar.gov.afip.dif.FEV1/'
        soap.message 'Auth' => @company.auth_data.auth_hash, 'PtoVta' => @company.pto_venta,
          'CbteTipo' => cbte_type
      end

      resp.to_hash[:fe_comp_ultimo_autorizado_response][:fe_comp_ultimo_autorizado_result][:cbte_nro].to_i + 1
    end

    # Fetches the possible document codes and names
    # @return [Hash]
    #
    def get_custom(operation)
      set_client
      resp = @client.call(operation) do |soap|
        soap.message 'Auth' => @company.auth_data.auth_hash
      end
      resp.to_hash
    end

    # Sets up the cliet to perform consults to the api
    #
    #
    def set_client
      opts = { wsdl: @company.auth_data.wsfe_url, ssl_version: :TLSv1_2 }.merge! Eafip.logger_options
      @client = Savon.client(opts)
    end
  end
end
