module Eafip
  # include ActiveModel::Model
  # include ActiveModel::Validations::Callbacks
  require 'eafip/reference'
  require 'open-uri'
  require 'tmpdir'
  class Company
    attr_accessor :cuit, :pto_venta, :documento, :pkey, :cert, :concepto, :moneda, :iva_cond,
                  :token, :sign, :openssl_bin, :environment

    def initialize(attrs)
      self.cuit = attrs[:cuit]
      self.pto_venta = attrs[:pto_venta]
      self.documento = attrs[:documento]
      self.pkey = attrs[:pkey]
      self.cert = attrs[:cert]
      self.concepto = attrs[:concepto]
      self.moneda = attrs[:moneda]
      self.iva_cond = attrs[:iva_cond]
      # self.openssl_bin = attrs[:openssl_bin]
      self.environment = attrs[:environment] || :test
      validate!
    end

    def wsaa
      @_wsaa ||= Eafip::Wsaa.new(self)
    end

    def auth_data
      @_auth_data ||= Eafip::AuthData.new(self)
    end

    def reference
      @_reference ||= Eafip::Reference.new(self)
    end

    def cert_path
      cert_or_download
    end

    def pkey_path
      pkey_or_download
    end

    private

    def validate!
      raise "Cuit (cuit) es requerido" if cuit.blank?
      raise "Punto de Venta (pto_venta) es requerido" if pto_venta.blank?
      raise "Tipo de Documento del Cliente (documento) es requerido" if documento.blank?
      raise "Clave Privada de Afip (pkey) es requerido" if pkey.blank?
      raise "Certificado de Afip (cert) es requerido" if cert.blank?
      raise "Concepto en Afip (concepto) es requerido" if concepto.blank?
      raise "Tipo de Moneda (moneda) es requerido" if moneda.blank?
      raise "Condicion Iva del Cliente (iva_cond) es requerido" if iva_cond.blank?
      raise "Ambiente de Facturacion, [:test o :produccion] (environment) es requerido" if environment.blank?
    end

    def cert_or_download
      if uri?(cert)
        download_cert unless File.exist?(cert_filename)
        cert_filename
      else
        cert
      end
    end

    def pkey_or_download
      if uri?(pkey)
        download_pkey unless File.exist?(pkey_filename)
        pkey_filename
      else
        pkey
      end
    end

    def download_cert
      download = open(cert)
      IO.copy_stream(download, cert_filename)
    end

    def download_pkey
      download = open(pkey)
      IO.copy_stream(download, pkey_filename)
    end

    def uri?(string)
      uri = URI.parse(string)
      %w( http https ).include?(uri.scheme)
    rescue URI::BadURIError
      false
    rescue URI::InvalidURIError
      false
    end

    def cert_filename
      name = File.basename(URI.parse(cert).path)
      fullname = "cert_#{cuit}_#{Time.new.strftime('%Y_%m_%d')}_#{name}"
      "#{Dir.tmpdir}/#{fullname}"
    end

    def pkey_filename
      name = File.basename(URI.parse(pkey).path)
      fullname = "pkey_#{cuit}_#{Time.new.strftime('%Y_%m_%d')}_#{name}"
      "#{Dir.tmpdir}/#{fullname}"
    end

  end
end