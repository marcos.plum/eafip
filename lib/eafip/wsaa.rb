# -*- encoding: utf-8 -*-
module Eafip
  # Authorization class. Handles interactions wiht the WSAA, to provide
  # valid key and signature that will last for a day.
  #

  class Wsaa
    attr_accessor :company

    def initialize(company)
      @company = company
    end

    def login
      tra   = build_tra
      cms   = build_cms(tra)
      req   = build_request(cms)
      auth  = call_wsaa(req)

      write_yaml(auth)
    end

    # Builds the xml for the 'Ticket de Requerimiento de Acceso'
    # @return [String] containing the request body
    #
    # rubocop:disable Metrics/MethodLength
    # uniqueId: Entero de 32 bits sin signo que junto con “generationTime” identifica el requerimiento.
    # generationTime: Momento en que fue generado el requerimiento. La tolerancia de aceptación  será de hasta 24 horas previas al requerimiento de acceso.
    # expirationTime: Momento en el que expira la solicitud. La tolerancia de aceptación será de hasta 24 horas posteriores al requerimiento de acceso.
    # service: Identificación del WSN para el cual se solicita el TA.

    # Generación del Ticket de Requerimiento de Acceso (TRA)
    def build_tra
      now = (Time.now) - 120
      @from = now.strftime('%FT%T%:z')
      @to   = (now + ((12 * 60 * 60))).strftime('%FT%T%:z') #12 horas
      @id   = now.strftime('%s')
      tra  = <<-EOF
<?xml version="1.0" encoding="UTF-8"?>
<loginTicketRequest version="1.0">
  <header>
    <uniqueId>#{ @id }</uniqueId>
    <generationTime>#{ @from }</generationTime>
    <expirationTime>#{ @to }</expirationTime>
  </header>
  <service>wsfe</service>
</loginTicketRequest>
EOF
      tra
    end
    # rubocop:enable Metrics/MethodLength
    # Builds the CMS
    # @return [String] cms
    #
    def build_cms(tra)
      # `echo '#{ tra }' |
      #   #{ @company.openssl_bin } cms -sign -in /dev/stdin -signer #{ @company.cert } -inkey #{ @company.pkey } \
      #   -nodetach -outform der |
      #   #{ @company.openssl_bin } base64 -e`
      `echo '#{ tra }' |
        #{ Eafip.openssl_bin } cms -sign -in /dev/stdin -signer #{ @company.cert_path } -inkey #{ @company.pkey_path } \
        -nodetach -outform der |
        #{ Eafip.openssl_bin } base64 -e`
    end

    # Builds the CMS request to log in to the server
    # @return [String] the cms body
    #
    def build_request(cms)
      # rubocop:disable Metrics/LineLength
      request = <<-XML
<?xml version="1.0" encoding="UTF-8"?>
<SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ns1="http://wsaa.view.sua.dvadac.desein.afip.gov">
  <SOAP-ENV:Body>
    <ns1:loginCms>
      <ns1:in0>
#{ cms }
      </ns1:in0>
    </ns1:loginCms>
  </SOAP-ENV:Body>
</SOAP-ENV:Envelope>
XML
      request
    end
    # rubocop:enable Metrics/LineLength

    # Calls the WSAA with the request built by build_request application/soap+xml; action=""' -d @- #{ @company.auth_data.wsaa_u
    # @return [Array] with the token and signature
    # En caso de encontrarse algún error, el mensaje SOAP devolverá un “SoapFault” conteniendo
    # código y descripción del error producido.
    def call_wsaa(req)
      response = `echo '#{ req }' |
        curl -k -s -H 'Content-Type: application/soap+xml; action=""' -d @- #{ @company.auth_data.wsaa_url }`

      response = CGI::unescapeHTML(response)
      response_login_errors(response)
      token = response.scan(%r{\<token\>(.+)\<\/token\>}).first.first
      sign = response.scan(%r{\<sign\>(.+)\<\/sign\>}).first.first
      [token, sign]
    end

    # Writes the token and signature to a YAML file in the /tmp directory
    #
    def write_yaml(certs)
      yml = <<-YML
token: #{certs[0]}
sign: #{certs[1]}
YML
      `echo '#{ yml }' > #{data_filename}`
    end

    def data_filename
      @_data_filename ||= "#{Dir.tmpdir}/eafip_#{ @company.cuit }_#{ Time.new.strftime('%Y_%m_%d') }.yml"
    end

    def response_login_errors(response)
      raise "No se obtuvo respuesta al realizar el Login" if response.blank?
      cms_error = response.scan(%r{\<faultstring\>(.+)\<\/faultstring\>})
      raise cms_error.first.first if cms_error.present?
      token_response = response.scan(%r{\<token\>(.+)\<\/token\>})
      raise "Error obteniendo el Token" if token_response.length != 1
      sign_response = response.scan(%r{\<sign\>(.+)\<\/sign\>})
      raise "Error obteniendo Sign" if sign_response.length != 1
    end

  end
end
