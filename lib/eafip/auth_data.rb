module Eafip
require 'eafip/constants'
  # This class handles authorization data
  #
  class AuthData

    attr_accessor :company

    def initialize(company)
      @company = company
      @wsaa = @company.wsaa
      @environment = @company.environment || :test
    end

    # Fetches WSAA Authorization Data to build the datafile for the day.
    # It requires the private key file and the certificate to exist and
    # to be configured as @company.pkey and @company.cert
    #
    def fetch
      unless @company.pkey.present? && File.exist?(@company.pkey_path)
        raise "Archivo de llave privada no encontrado en #{ @company.pkey_path }"
      end
      unless @company.cert.present? && File.exist?(@company.cert_path)
        raise "Archivo certificado no encontrado en #{ @company.cert_path }"
      end
      # Si no se ha obtenido el token and sign
      unless File.exist?(@wsaa.data_filename)
        begin
          @wsaa.login
        rescue Exception => e
          raise "Error realizando la Autenticación con el Servicio de AFIP: #{e.message}"
        end
      end

      data = YAML.load_file(@wsaa.data_filename)
      @company.token = data["token"]
      @company.sign = data["sign"]
    end

    # Returns the authorization hash, containing the Token, Signature and Cuit
    # @return [Hash]
    #
    def auth_hash
      fetch if @company.token.blank? || @company.sign.blank?
      { 'Token' => @company.token, 'Sign' => @company.sign, 'Cuit' => @company.cuit }
    end

    # Returns the right wsaa url for the specific environment
    # @return [String]
    #
    def wsaa_url
      # check_environment!
      Eafip::URLS[@environment][:wsaa]
    end

    # Returns the right wsfe url for the specific environment
    # @return [String]
    #
    def wsfe_url
      # check_environment!
      Eafip::URLS[@environment][:wsfe]
    end

    def check_environment!
      raise 'Environment not set, fail in check_environment!.' unless Eafip::URLS.keys.include?(@environment)
    end
  end
end
